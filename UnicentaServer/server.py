#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
from RoomPlanner import bootstrap

def browse():
	print "browse: %s" % cherrypy.url()
	try:
		import webbrowser
		webbrowser.open("%s" % cherrypy.url())
	except ImportError:
		print "ImportError"

def start():
	bootstrap()
	cherrypy.engine.subscribe('start', browse)
	#cherrypy.engine.signals.subscribe('start', browse)
	cherrypy.engine.start()
	cherrypy.engine.block()

def init():
	bootstrap()
	start()

# debugging purpose, e.g. run with PyDev debugger
if __name__ == '__main__':
	start()#init()	# The main tasks have been moved to init to allow it to be called externally. Possibly move booststrap to start and just call start? :/ 
