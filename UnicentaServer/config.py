# -*- coding: utf-8 -*-


import os


path   = os.path.abspath(os.path.dirname(__file__))
config = {
  'global' : {
    'server.socket_host' : '0.0.0.0',
    'server.socket_port' : 8080,
    'server.thread_pool' : 16,

    'engine.autoreload.on' : True,

    'tools.trailing_slash.on' : False,
    'log.access_file': './access.log',
    'log.error_file': './error.log'
  },
  '/': {
  'tools.sessions.on': True,
  'tools.staticdir.root': os.path.abspath(os.getcwd())
  },
  '/resource' : {
    'tools.staticdir.on'  : True,
    'tools.staticdir.dir' : os.path.join(path, 'assets')
  }

}
'''
  '/generator': {
  'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
  'tools.response_headers.on': True,
  'tools.response_headers.headers': [('Content-Type', 'text/plain')],
  },'''