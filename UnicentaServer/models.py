""" Test Cherry Pi

Author: Martyn Bristow
Contact: martyn.bristow@gmail.com
Copyright Martyn Bristow 2015

Requires:
CherryPy
"""

from sqlalchemy import Column, Integer, String
#from yourapplication.database import Base
#from database import Base
from datatime import now

class User(Base):
	""" Application Users
	"""
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(20), unique=True)
    name = Column(String(50), unique=True)
    email = Column(String(120), unique=True)
    password = Column(String(50), unique=False)
    priv = Column(Int, unique=False)
    enable = Column(Bool, unique=False)
    lock = Column(Bool, unique=False)
    lastLogin = Column(DateTime, unique=False)
    create = Column(DateTime, unique=False)

    def __init__(self, username=None, name=None, email=None, password=None):
        self.username = username
        self.email = email
        self.name = name
        self.password = password
        self.priv = priv
        self.enable = True
        self.lock = True
        self.create = datetime.datetime.now()
        self.lastLogin = 0

    def __repr__(self):
        return '<User %r>' % (self.name)

class Sessions(Base):
    """ Application Users
    """
    __tablename__ = 'sessions'
    id = Column(Integer, primary_key=True)
    userid = Column(ID, unique=True)
    name = Column(String(50), unique=True)
    email = Column(String(120), unique=True)
    password = Column(String(50), unique=False)
    priv = Column(Int, unique=False)
    enable = Column(Bool, unique=False)
    lock = Column(Bool, unique=False)
    lastLogin = Column(DateTime, unique=False)
    create = Column(DateTime, unique=False)

    def __init__(self, username=None, name=None, email=None, password=None):
        self.username = username
        self.email = email
        self.name = name
        self.password = password
        self.priv = priv
        self.enable = True
        self.lock = True
        self.create = datetime.datetime.now()
        self.lastLogin = 0

    def __repr__(self):
        return '<Session %r>' % (self.name)

class Locations(Base):
    """ Application Users
    """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(20), unique=True)
    name = Column(String(50), unique=True)
    email = Column(String(120), unique=True)
    password = Column(String(50), unique=False)
    priv = Column(Int, unique=False)
    enable = Column(Bool, unique=False)
    lock = Column(Bool, unique=False)
    lastLogin = Column(DateTime, unique=False)
    create = Column(DateTime, unique=False)

    def __init__(self, username=None, name=None, email=None, password=None):
        self.username = username
        self.email = email
        self.name = name
        self.password = password
        self.priv = priv
        self.enable = True
        self.lock = True
        self.create = datetime.datetime.now()
        self.lastLogin = 0

    def __repr__(self):
        return '<Location %r>' % (self.name)

class Rooms(Base):
    """ Application Users
    """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(20), unique=True)
    name = Column(String(50), unique=True)
    email = Column(String(120), unique=True)
    password = Column(String(50), unique=False)
    priv = Column(Int, unique=False)
    enable = Column(Bool, unique=False)
    lock = Column(Bool, unique=False)
    lastLogin = Column(DateTime, unique=False)
    create = Column(DateTime, unique=False)

    def __init__(self, username=None, name=None, email=None, password=None):
        self.username = username
        self.email = email
        self.name = name
        self.password = password
        self.priv = priv
        self.enable = True
        self.lock = True
        self.create = datetime.datetime.now()
        self.lastLogin = 0

    def __repr__(self):
        return '<Room %r>' % (self.name)

class Clients(Base):
    """ Application Users
    """
    __tablename__ = 'client'
    id = Column(Integer, primary_key=True)
    username = Column(String(20), unique=True)
    name = Column(String(50), unique=True)
    email = Column(String(120), unique=True)
    password = Column(String(50), unique=False)
    priv = Column(Int, unique=False)
    enable = Column(Bool, unique=False)
    lock = Column(Bool, unique=False)
    lastLogin = Column(DateTime, unique=False)
    create = Column(DateTime, unique=False)

    def __init__(self, username=None, name=None, email=None, password=None):
        self.username = username
        self.email = email
        self.name = name
        self.password = password
        self.priv = priv
        self.enable = True
        self.lock = True
        self.create = datetime.datetime.now()
        self.lastLogin = 0

    def __repr__(self):
        return '<Client %r>' % (self.name)

class Event(Base):
    """ Events
    """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(20), unique=True)
    name = Column(String(50), unique=True)
    email = Column(String(120), unique=True)
    password = Column(String(50), unique=False)
    priv = Column(Int, unique=False)
    enable = Column(Bool, unique=False)
    lock = Column(Bool, unique=False)
    lastLogin = Column(DateTime, unique=False)
    create = Column(DateTime, unique=False)

    def __init__(self, username=None, name=None, email=None, password=None):
        self.username = username
        self.email = email
        self.name = name
        self.password = password
        self.priv = priv
        self.enable = True
        self.lock = True
        self.create = datetime.datetime.now()
        self.lastLogin = 0

    def __repr__(self):
        return '<Event %r>' % (self.name)

class Layout(Base):
    """ Application Users
    """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(20), unique=True)
    name = Column(String(50), unique=True)
    email = Column(String(120), unique=True)
    password = Column(String(50), unique=False)
    priv = Column(Int, unique=False)
    enable = Column(Bool, unique=False)
    lock = Column(Bool, unique=False)
    lastLogin = Column(DateTime, unique=False)
    create = Column(DateTime, unique=False)

    def __init__(self, username=None, name=None, email=None, password=None):
        self.username = username
        self.email = email
        self.name = name
        self.password = password
        self.priv = priv
        self.enable = True
        self.lock = True
        self.create = datetime.datetime.now()
        self.lastLogin = 0

    def __repr__(self):
        return '<Layout %r>' % (self.name)