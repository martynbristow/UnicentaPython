# -*- coding: utf-8 -*-

import cherrypy
from cherrypy.lib.static import serve_file
from jinja2 import Environment, PackageLoader
env = Environment(loader=PackageLoader('UnicentaServer', 'views'))
# check the above are used
AppName="POS"
from UnicentaPythonAPI import database
class Index(object):
	
	@cherrypy.expose
	def index(self):
		return database.Cash.opencash()

class Setup(object):
	def index():
		pass
	
	def install():
		pass

	def reset():
		pass

class System(object):
	def index():
		pass
	
	def shutdown():
		pass

	def reset():
		pass

class Sales(object):
	''' Application Management
	'''

	def index():
		pass

class Reporting(object):
	''' Application Management
	'''

	def index():
		pass

class Manage(object):
	''' Application Management
	'''

	def index():
		pass

def errorPage(status, message, **kwargs):
	info = {
	'status_code': "%s" % status,
	'status_text': "%s" % message
	}
	return env.get_template('error.html').render(info, css=css, js=js, title=AppName)
