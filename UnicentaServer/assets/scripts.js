/* General Scripts
*/
$(document).ready(function() {

	/* This is basic - uses default settings */
	
	$("a.fancy").fancybox();
	
	/* Using custom settings */
	
	$("a.inline").fancybox({
		'hideOnContentClick': true
	});

	/* Apply fancybox to multiple items */
	
	$("a.fancygroup").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false
	});

	$(function() {
    	$("#toolbar").accordion({
    		collapsible: true,
    		active:false,
    		heightStyle: "content"
    		});
	});
    
});

function download(a, filename, text) {
    a.setAttribute('href', 'data:application/octet-stream,' + encodeURIComponent(text));
    a.setAttribute('download', filename);
    a.click();
}
