#!/usr/bin/env python
# -*- coding: utf-8 -*-
# UnicentaPython HTTP Server Startup

appname = "UnicentaServer"

import cherrypy
from UnicentaServer import bootstrap
from UnicentaPythonAPI import uri as dburi

def browse():
	""" Open a webbrowser to the URL of the server
	Useful when running on a local machine
	"""
	print "Browse: %s" % cherrypy.url()
	try:
		import webbrowser
		webbrowser.open("%s" % cherrypy.url())
	except ImportError:
		print "ImportError"

def start():
    """ Start the CherryPy webserver
    """
    try:
        bootstrap()
        cherrypy.engine.subscribe('start', browse)
        from UnicentaServer.ext import SAEnginePlugin as SAPlugin
        SAPlugin(cherrypy.engine, dburi).subscribe()
        #Register the SQLAlchemy tool
        from UnicentaServer.ext import SATool as SATool
        cherrypy.tools.db = SATool()
        cherrypy.engine.start()
        cherrypy.engine.block()
    except RuntimeError:
        raise
    """except Exception as e:	# Fix the exceptions
		print e
		print "Sorry requested is in use, maybe your already running %s\n%s" % (appname, e)
    """

def init():
	bootstrap()
	start()

# debugging purpose, e.g. run with PyDev debugger
if __name__ == '__main__':
	try:
		start()
	except KeyboardInterrupt:
		pass
