===========
RoomPlanner
===========

.. |date| date::
.. |time| date:: %H:%M

.. footer:: Updated: |date| at |time|

.. header:: Room Planner Development Plan.

:Info: http://martynbristow.co.uk/projects/roomplanner
:Author: Martyn Bristow <martyn@martynbristow.co.uk>

:Version: 0.1 of |date|

:Description: Room Planner

Contents
========

.. contents:: :depth: 2


Description
===========

A web-based layout designer using HTML5 or JQuery UI as the front end, and a Python webserver.

Requirements
============

Functional Requirments
----------------------

Non-Functional Requirments
--------------------------

Timeline
========

Schedule
--------