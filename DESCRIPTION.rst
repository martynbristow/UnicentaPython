===========
RoomPlanner
===========

.. |date| date::
.. |time| date:: %H:%M

.. footer:: Updated: |date| at |time|

.. header:: Room Planner

:Info: http://martynbristow.co.uk/projects/roomplanner
:Author: Martyn Bristow <martyn@martynbristow.co.uk>

:Version: 0.1 of |date|

:Description: Room Planner

Contents
========

.. contents:: :depth: 2


Room Planner WebApp
===================

A Layout Designer in your web browser, running from Python
Uses JQueryUI or HTML5 depening on your device.

A python installation is required, along with a number of Python depencies which are installed automatically from the internet and a number of preloaded packages.

Installation
------------

To setup the software simply run::

	python setup.py

This will build and install the Python package on your system.

If you don't have 'root' access to the machine use the following command::

	python setup.py --local

Startup
-------

To start up the room planner server click the icon or in a terminal run::

	``python server.py``

Open the url http://localhost:8080/