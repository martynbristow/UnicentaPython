===================
Room Planner Readme
===================

A sample project that exists as an aid to the `Python Packaging User Guide
<https://packaging.python.org>`_'s `Tutorial on Packaging and Distributing
Projects <https://packaging.python.org/en/latest/distributing.html>`_.

Test Install
============

export PYTHONPATH=$PYTHONPATH:$HOME/test/lib/python2.7/site-packages/
python setup.py install --prefix=~/test

Usage
=====

Startup

.. code::

$ roomplanner

Shutdown Server
===============

To safely shutdown the server, open the url: http://server:8080/shutdown/now

.. code::

$ roomplanner