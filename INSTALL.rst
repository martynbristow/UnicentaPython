===========
RoomPlanner
===========

.. |date| date::
.. |time| date:: %H:%M

.. footer:: Updated: |date| at |time|

.. header:: Room Planner Installer

:Info: http://martynbristow.co.uk/projects/roomplanner
:Author: Martyn Bristow <martyn@martynbristow.co.uk>

:Version: 0.1 of |date|

:Description: Room Planner

Contents
========

.. contents:: :depth: 2


Room Planner WebApp
===================

A web-based layout designer using HTML5 or JQuery UI as the front end, and a Python webserver.

Installation
------------

``python setup.py``

Startup
-------

To start the server simply type: ``roomplanner`` at the command line.

Alternatively: ``python server.py``

Open the url localhost:8080