# README #

Welcome to UnicentaPython, it provides a simplified database API in Python, and accompanying REST API allowing you access to Unicenta without a Java client.

## Summary ##

### Features ###
* Dashboard - A current view of how your business is doing right now
* Management - Perform 
* Reporting

## Versions ##

## Installation ##

```
#!python

cd <folder>
python setup.py install
```


### Database Connection ###
Set your 

#### Sample Database ####
We provide some sample MySQL database with generic data to get you started.
Do not use these data to store real data because they are frequently re-imaged without advance warning and are only to be used as testing purposes.
Simply connect to your own MySQL/PostGRES database.
We provide some populated database scripts to get you started.

`ssh <user>@martynbristow.no-ip.org -L 3306:localhost:3306`

Edit your: `.ssh/config` adding, to enable an SSH Alias

```
#!

Host data_sql
        User martyn
        HostName martynbristow.no-ip.org
        LocalForward 3306 127.0.0.1:3306
```


Then use `localhost:3306` as your database URL will work

Links:
Database