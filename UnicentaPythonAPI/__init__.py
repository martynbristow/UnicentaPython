""" Unicenta OPOS PythonAPI
Create an API Instance

Description: 
Author: Martyn Bristow
Homepage: http://
Copyright:
License: GPLv3
"""

import logging

import Config

from sqlalchemy import MetaData
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.exc import InterfaceError
from datetime import datetime
from sqlalchemy import func
from sqlalchemy.sql import label
import logging

config = Config.loader('development')

uri = "%s:%s@%s/%s" % (config['user'], config['password'], config['host'], config['database'])
uri = "mysql+mysqlconnector://%s"

logging = logging.getLogger('Unicenta')
# Determine the Database URI

class Connection(object):
    """ Create the database connection
    """
    def __init__(self):
        self.base = declarative_base()
        self.engine = create_engine(self.dburi, **self.create_kwargs)

    @classmethod
    def create(cls, dburi, **kw):
        self.dburi = dburi
        self.create_kwargs = kw
        self = cls()
    
    def stop(self):
    	pass
        #self.sa_engine.dispose()
        #self.sa_engine = None

def setup():
    conn = Connection.connect(uri, pool_recycle=3600)

class Database():
    """
    """
    
    def __init__(self, conn):
        self.connection = conn
        metadata = MetaData(conn.engine)
        Base = automap_base()
        try:
            Base.prepare(conn.engine, reflect=True)
            metadata.reflect()
        except InterfaceError:
            raise RuntimeError('Unable to connect to database')

        Tables = {table.name: table for table in metadata.sorted_tables}
        Session = sessionmaker(bind=engine)
        session = Session()