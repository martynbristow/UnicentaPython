""" UnicentaOPOS Python Interface Configuration

All configured settings should be set here

** Usage **

import Config
"""
# Database Settings - Migrate to CFG
database = {
	'default':
	{
	'user':'martyn',
	'password':'ncis99Secure',
	'host':'localhost',
	'database':'AughtonVillageHall',
	'type':'mysql'
	},
	'development':
	{
	'user':'martyn',
	'password':'',
	'host':'localhost',
	'database':'AughtonVillageHall',
	'type':'mysql'
	}
}
# Drivers available
drivers = {
	'mysql':'mysql+mysqlconnector',
	'postgres': None
}

_server = "{driver}://{username}:{password}@{server}:{port}/{database}"
_filedb = "sqlite:///{path}"


class ConfigError(Exception):
	""" Configuration Errors
	"""
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

class Config(object):
	""" Initialise the Config
	Read from cfg file
	"""
	def __init__(self, config='default'):
		try:
			self.config = database[config]
		except KeyError:
			raise ConfigError('Unable to load the configuation.\nConfiguration %s isn`t known')
		#self.conn = "%s://%s:%s@%s/%s" % (drivers[database[config]['type']], database[config]['user'], database[config]['password'], database[config][host], database[config][database])
		self.dburi = _server.format(self.config)

def loader(config='default'):
	return database[config]
