""" POS Database Models
Author: Martyn Bristow
Description: Autoload database models from a connection
"""

from sqlalchemy import MetaData
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.exc import InterfaceError
from datetime import datetime
from sqlalchemy import func
from sqlalchemy.sql import label
import logging

# Construct URI -> Driver ?
class Database(object):
	""" Database container class
	This is a static object and will percist through every instance
	"""
	# Switch to URI in the long run
	engine = create_engine('mysql+mysqlconnector://%s' % uri, pool_recycle=3600)

engine = create_engine('mysql+mysqlconnector://%s' % uri, pool_recycle=3600)

metadata = MetaData(engine)
Base = automap_base()
try:
	Base.prepare(engine, reflect=True)
	metadata.reflect()
except InterfaceError:
	raise RuntimeError('Unable to connect to database')


Tables = {table.name: table for table in metadata.sorted_tables}

#tablenames = map(lambda table: table.name, Tables)
#queries.database.Base.classes.PEOPLE

#session = create_session(bind=engine)
Session = sessionmaker(bind=engine)
session = Session()

class Role(Base.classes['ROLE']):
	pass

class People(Base.classes['PEOPLE']):
	"""
	"""
	@classmethod
	def auth(cls, username, password=None):
		return session.query(cls.APPPASSWORD).filter(cls.NAME == username).one()[0] == password
	@classmethod
	def list(cls):
		return session.query(cls.NAME).all()
	@classmethod
	def create(cls, **kwarg):
		self = Base.classes['PEOPLE'](NAME=NAME, APPPASSWORD="", VISIBLE=0)
		self.ROLE = setRole()
		self.ID = makeKey()
		session.add(self)
	def enable(id):
		pass
	def disable(id):
		pass
	def delete(id):
		pass
	def update(userid):
		pass
	def setpassword(userid):
		pass
	def read(userid):
		return session.query(self.__table__).filter(self.__table__).all()

class Shifts():
	pass

class Products(Base.classes['CATEGORIES']):
	pass

class Products(Base.classes['PRODUCTS']):
	pass

class Customer(Base.classes['CUSTOMERS']):
	pass

class Cash(Base.classes['CLOSEDCASH']):
	"""
	"""
	def query(self):
	    """ Generate a closed cash query with no filters
	    user outerjoin to relax the join condition
	    """
	    return session.query(
	        Base.classes['CLOSEDCASH'].MONEY,
	        label('total', func.sum(Base.classes['PAYMENTS'].TOTAL)),
	        Base.classes['CLOSEDCASH'].DATEEND,
	        Base.classes['PAYMENTS'].PAYMENT,
	        ).join(Base.classes['RECEIPTS'], Base.classes['CLOSEDCASH'].MONEY == Base.classes['RECEIPTS'].MONEY
	        ).join(Base.classes['PAYMENTS'], Base.classes['PAYMENTS'].RECEIPT == Base.classes['RECEIPTS'].ID
	        ).group_by(Base.classes['CLOSEDCASH'].MONEY).group_by(Base.classes['PAYMENTS'].PAYMENT)
	@classmethod
	def cashclosed(cls, start=None, end=None):
		""" Get the closed between start and end
		"""
		if start is None:
			start = datetime.fromtimestamp(0)
		if end is None:
			end = datetime.now()
		assert start < end
		return _closedcash().filter(Base.classes['CLOSEDCASH'].DATEEND<=end, Base.classes['CLOSEDCASH'].DATESTART >= start
	        ).all()
	@classmethod
	def opencash(cls):
		""" Get the open cashes
		"""
		return cls().query().filter(Base.classes['CLOSEDCASH'].DATEEND==None).all()


class Cash(Base.classes['RESOURCES']):
	pass
	
def getVersion():
	return session.query(Base.classes['APPLICATIONS'])
"""
['SHAREDTICKETS', 'TAXLINES', 'ROLES', 'PEOPLE', 'STOCKDIARY', 'TICKETSNUM_PAYMENT', 'PRODUCTS_CAT', 'CSVIMPORT', 'ATTRIBUTEUSE', 'APPLICATIONS', 'FLOORS', 'TAXCUSTCATEGORIES', 'PRODUCTS_COM', 'ATTRIBUTESETINSTANCE', 'CATEGORIES', 'CUSTOMERS', 'LEAVES', 'LOCATIONS', 'PRODUCTS', 'PAYMENTS', 'RESERVATIONS', 'SHIFT_BREAKS', 'TICKETS', 'users', 'ATTRIBUTEINSTANCE', 'BREAKS', 'ATTRIBUTESET', 'STOCKLEVEL', 'TAXES', 'TAXCATEGORIES', 'RESOURCES', 'RECEIPTS', 'ATTRIBUTEVALUE', 'PLACES', 'ATTRIBUTE', 'SHIFTS', 'CLOSEDCASH', 'THIRDPARTIES', 'ci_sessions', 'TICKETLINES']
"""
if __name__ == "__main__":
	pass