""" UnicentaPython:
Queries
Description: Common Queries using the ORM

"""
'''Price List
SELECT PRODUCTS.CODE, PRODUCTS.NAME, PRODUCTS.PRICEBUY, PRODUCTS.PRICESELL, CATEGORIES.NAME AS CATEGORYNAME, PRODUCTS.ATTRIBUTES FROM PRODUCTS LEFT OUTER JOIN CATEGORIES ON PRODUCTS.CATEGORY = CATEGORIES.ID LEFT OUTER JOIN TAXCATEGORIES TC ON PRODUCTS.TAXCAT = TC.ID ORDER BY CATEGORIES.NAME, PRODUCTS.NAME

Add User
INSERT INTO PEOPLE (NAME, VISIBLE, ID, ROLE) VALUES ('NEW', 1, '4', '0')'''

import database
import logging

from datetime import datetime
from sqlalchemy import func
from sqlalchemy.sql import label

Tables = database.Tables

def _closedcash():
    """ Generate a closed cash query with no filters
    user outerjoin to relax the join condition
    """
    return database.session.query(
        Tables['CLOSEDCASH'].columns.MONEY,
        label('total', func.sum(Tables['PAYMENTS'].columns.TOTAL)),
        Tables['CLOSEDCASH'].columns.DATEEND,
        Tables['PAYMENTS'].columns.PAYMENT,
        ).join(Tables['RECEIPTS'], Tables['CLOSEDCASH'].columns.MONEY == Tables['RECEIPTS'].columns.MONEY).join(Tables['PAYMENTS'], Tables['PAYMENTS'].columns.RECEIPT == Tables['RECEIPTS'].columns.ID).group_by(Tables['CLOSEDCASH'].columns.MONEY).group_by(Tables['PAYMENTS'].columns.PAYMENT)

def cashclosed(start=None, end=None):
	""" Get the closed between start and end
	"""
	if start is None:
		start = datetime.fromtimestamp(0)
	if end is None:
		end = datetime.now()
	assert start < end
	return _closedcash().filter(Tables['CLOSEDCASH'].columns.DATEEND<=end, Tables['CLOSEDCASH'].columns.DATESTART >= start
        ).all()

def opencash():
	""" Get the open cashes
	"""
	return _closedcash().filter(Tables['CLOSEDCASH'].columns.DATEEND==None).all()

def price_list(**kwarg):
	""" Get the price list
	SELECT PRODUCTS.CODE, PRODUCTS.NAME, PRODUCTS.PRICEBUY, PRODUCTS.PRICESELL, CATEGORIES.NAME AS CATEGORYNAME, PRODUCTS.ATTRIBUTES FROM PRODUCTS LEFT OUTER JOIN CATEGORIES ON PRODUCTS.CATEGORY = CATEGORIES.ID LEFT OUTER JOIN TAXCATEGORIES TC ON PRODUCTS.TAXCAT = TC.ID ORDER BY CATEGORIES.NAME, PRODUCTS.NAME

	"""
	pass

def produt_sales(start=None, end=None):
	""" Get the price list
	"""
	pass
#SELECT CLOSEDCASH.money, sum(PAYMENTS.`total`), CLOSEDCASH.`DATESTART`, CLOSEDCASH.`DATEEND` from CLOSEDCASH left join RECEIPTS on CLOSEDCASH.MONEY = RECEIPTS.MONEY left join PAYMENTS on PAYMENTS.RECEIPT = RECEIPTS.ID group by CLOSEDCASH.MONEY
