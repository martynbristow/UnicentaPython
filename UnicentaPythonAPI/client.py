""" Unicenta Client Settings

Settings used on a POS terminal

"""
import logging

logger = logging.getLogger('client')

class Properties(object):

	def __init__(self):
		pass

	@classmethod
	def fromFile(cls, path):
		self = cls()
		self.path = path
		try:
			with open(self.path) as f:
				self.data = map(lambda s: s.strip(), f.readlines())
		except IOError:
			pass
		return  self


if __name__ == "__main__":
	import os
	path = os.path.expanduser('~/unicentaopos.properties')
	Properties.fromFile(path)